package com.shashank.assignment.apicalling;

import com.shashank.assignment.model.DataModelResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("cardData")
    Call<ArrayList<DataModelResponse>> getDataModelResponse();
}
