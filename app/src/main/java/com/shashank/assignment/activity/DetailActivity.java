package com.shashank.assignment.activity;

import android.app.AlertDialog;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.shashank.assignment.R;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    BottomSheetBehavior bottomSheetBehavior;
    private LinearLayout bottom_sheet;
    private Button btnExpand;
    private Button btnHide;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinator_layout);
        initView();
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Toast.makeText(DetailActivity.this, "hidden", Toast.LENGTH_SHORT).show();
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Toast.makeText(DetailActivity.this, "collapsed", Toast.LENGTH_SHORT).show();
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Toast.makeText(DetailActivity.this, "dragging", Toast.LENGTH_SHORT).show();
                        break;
                    case BottomSheetBehavior.STATE_HALF_EXPANDED:
                        Toast.makeText(DetailActivity.this, "half expanded", Toast.LENGTH_SHORT).show();
                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
                Log.d("new state", "onStateChanged: " + newState);
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        btnExpand.setOnClickListener(this);
 /*       AlertDialog.Builder builder5 = new AlertDialog.Builder(this);

        builder5.setMessage("version 3.2 new")
                .setCancelable(true)
                .setPositiveButton("show detail", (dialog, id) -> {
                    dialog.dismiss();
                });
        AlertDialog alert5 = builder5.create();
        alert5.show();
        Toast.makeText(this, "detail activity", Toast.LENGTH_SHORT).show();
 */   }

    private void initView() {
        bottom_sheet = findViewById(R.id.bottom_sheet);
        btnExpand = findViewById(R.id.btn_expand);
        btnHide = findViewById(R.id.btn_hide);

    }

    @Override
    public void onClick(View v) {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            btnExpand.setText("Collapse BottomSheet");
        } else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            btnExpand.setText("Expand BottomSheet");

        }
    }
}