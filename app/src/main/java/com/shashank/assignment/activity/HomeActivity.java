package com.shashank.assignment.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.tasks.Task;
import com.shashank.assignment.R;
import com.shashank.assignment.adapter.HomeAdapter;
import com.shashank.assignment.apicalling.ApiClient;
import com.shashank.assignment.apicalling.ApiInterface;
import com.shashank.assignment.model.DataModelResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    //todo RC_APP_UPDATE = 17362 is given by develor should be changed in runtime
    private static final int RC_APP_UPDATE = 17362;

    private static final String TAG = HomeActivity.class.getSimpleName();
    private Task<AppUpdateInfo> appUpdateInfoTask;
    private AppUpdateManager appUpdateManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_home);
        //        checkForUpdate();//on onCreate
//        appUpdateManager= AppUpdateManagerFactory.create(this);


//        callApi();
    }

    private void callApi() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<DataModelResponse>> call = apiInterface.getDataModelResponse();

        call.enqueue(new Callback<ArrayList<DataModelResponse>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<DataModelResponse>> call, @NonNull Response<ArrayList<DataModelResponse>> response) {
                ArrayList<DataModelResponse> dataModelResponseArrayList = response.body();
                initView(dataModelResponseArrayList);
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<DataModelResponse>> call, @NonNull Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    private void initView(ArrayList<DataModelResponse> dataModelResponseArrayList) {
        RecyclerView rv = findViewById(R.id.recyclerView);
        TextView tvNoDataFound = findViewById(R.id.tv_no_data_found);

        if (dataModelResponseArrayList == null || dataModelResponseArrayList.size() <= 0) {
            rv.setVisibility(View.GONE);
            tvNoDataFound.setVisibility(View.VISIBLE);
        } else {
            rv.setVisibility(View.VISIBLE);
            tvNoDataFound.setVisibility(View.GONE);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HomeActivity.this);
            rv.setLayoutManager(layoutManager);

            HomeAdapter homeAdapter = new HomeAdapter(HomeActivity.this, dataModelResponseArrayList);
            rv.setAdapter(homeAdapter);
        }
    }

   /* private void checkForUpdate() {
        Toast.makeText(this, "in app Update is checking", Toast.LENGTH_LONG).show();

        mAppUpdateManager = AppUpdateManagerFactory.create(this);
        mAppUpdateManager.registerListener(this);

        appUpdateInfoTask = mAppUpdateManager.getAppUpdateInfo();
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                startInAppUpdate(appUpdateInfo);
            }*//* else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(HomeActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, 30000);
            }
*//*
        });
    }
*/
  /*  private void startInAppUpdate(AppUpdateInfo appUpdateInfo) {
        Toast.makeText(HomeActivity.this, "Start in app update", Toast.LENGTH_LONG).show();
        try {
            mAppUpdateManager.startUpdateFlowForResult(appUpdateInfo,
                    AppUpdateType.FLEXIBLE,
                    this,
                    RC_APP_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //showToast("Data:" + data.getExtras().toString());
        showToast("Code:" + requestCode);
        if (requestCode == RC_APP_UPDATE) {
            switch (resultCode){
                case Activity.RESULT_OK:
                    Toast.makeText(HomeActivity.this, "RESULT OK", Toast.LENGTH_LONG).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("User's approval")
                            .setCancelable(true)
                            .setPositiveButton("Okay", (dialog, id) -> {
                                dialog.dismiss();
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(HomeActivity.this, "user's deny" + resultCode, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Update flow failed! Result code: " + resultCode);
                    break;

                case ActivityResult.RESULT_IN_APP_UPDATE_FAILED:
                    Toast.makeText(HomeActivity.this, "Some other reason update failed" + resultCode, Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    private void showToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAppUpdateManager != null) {
            Task<AppUpdateInfo> appUpdateInfoTask = mAppUpdateManager.getAppUpdateInfo();

            appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
                *//*if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                    Toast.makeText(this, "update downloaded click okay to restart", Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("update is downloaded! click restart for new features")
                            .setCancelable(true)
                            .setPositiveButton("Restart", (dialog, id) -> {
                                mAppUpdateManager.completeUpdate();
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }*//*

                if(appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS){
                    try {
                        mAppUpdateManager.startUpdateFlowForResult(
                                appUpdateInfo,
                                AppUpdateType.FLEXIBLE,
                                this,
                                RC_APP_UPDATE
                        );
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAppUpdateManager.unregisterListener(this);
    }

*/
    /*@Override
    public void onStateUpdate(InstallState state) {
        switch (state.installStatus()) {
            case InstallStatus.DOWNLOADING:
                Toast.makeText(this, "Downloading", Toast.LENGTH_LONG).show();
                Log.d(TAG, String.valueOf(state.installStatus()));
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
//                Utility.showDialog(SplashActivity.this, String.valueOf(state.installStatus()));
                builder2.setMessage("update status downloading.")
                        .setCancelable(true)
                        .setPositiveButton("use app", (dialog, id) -> {
                           });
                AlertDialog alert2 = builder2.create();
                alert2.show();
                //show downloading UI
                break;

            case InstallStatus.DOWNLOADED:
                Toast.makeText(this, "Downloaded", Toast.LENGTH_LONG).show();
                Log.d(TAG, String.valueOf(state.installStatus()));
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("update is downloaded! click restart for new features")
                        .setCancelable(true)
                        .setPositiveButton("Restart", (dialog, id) -> {
                            mAppUpdateManager.completeUpdate();

                        });
                AlertDialog alert = builder.create();
                alert.show();
                break;

            case InstallStatus.FAILED:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "status Failed", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setMessage("In app update status Failed. please try again.")
                        .setCancelable(true)
                        .setPositiveButton("try again", (dialog, id) -> {
                            mAppUpdateManager.unregisterListener(this);
                            checkForUpdate(); //onStateUpdate case Failed
                            dialog.dismiss();
                        }).setNegativeButton("cancel", (dialog, which) -> {
                    dialog.dismiss();
                });

                AlertDialog alert1 = builder1.create();
                alert1.show();
                //show failed UI/handle failed updated
                //start again

                break;

            case InstallStatus.CANCELED:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update cancelled", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder5 = new AlertDialog.Builder(this);
                builder5.setMessage("In app update status cancelled.")
                        .setCancelable(true)
                        .setPositiveButton("go home", (dialog, id) -> {
                            mAppUpdateManager.unregisterListener(this);
                            dialog.dismiss();
                        });
                AlertDialog alert5 = builder5.create();
                alert5.show();

                //show canceled UI/handle failed update
                //open app
                break;

            case InstallStatus.INSTALLED:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update installed", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder4 = new AlertDialog.Builder(this);
                builder4.setMessage("In app update status Installed.")
                        .setCancelable(true)
                        .setPositiveButton("back to home", (dialog, id) -> {
                            dialog.dismiss();
                        });
                AlertDialog alert4 = builder4.create();
                alert4.show();

                break;

            case InstallStatus.INSTALLING:
                Toast.makeText(this, "update installing", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder6 = new AlertDialog.Builder(this);
                builder6.setMessage("In app update status Installing.")
                        .setCancelable(true)
                        .setPositiveButton("go home", (dialog, id) -> {
                            dialog.dismiss();
                        });
                AlertDialog alert6 = builder6.create();
                alert6.show();


                Log.d(TAG, String.valueOf(state.installStatus()));
                break;

            case InstallStatus.PENDING:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update pending", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder7 = new AlertDialog.Builder(this);
                builder7.setMessage("In app update status pending.")
                        .setCancelable(true)
                        .setPositiveButton("dialog dismiss", (dialog, id) -> {
                            dialog.dismiss();
                        });
                AlertDialog alert7 = builder7.create();
                alert7.show();

                break;

            case InstallStatus.REQUIRES_UI_INTENT:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update Requires UI intent", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder8 = new AlertDialog.Builder(this);
                builder8.setMessage("update Requires UI intent.")
                        .setCancelable(true)
                        .setPositiveButton("go home", (dialog, id) -> {

                            dialog.dismiss();

                        });
                AlertDialog alert8 = builder8.create();
                alert8.show();

                break;

            case InstallStatus.UNKNOWN:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update unknown", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder9 = new AlertDialog.Builder(this);
                builder9.setMessage("state unknown.")
                        .setCancelable(true)
                        .setPositiveButton("go home", (dialog, id) -> {
                            dialog.dismiss();
                        });
                AlertDialog alert9 = builder9.create();
                alert9.show();


                break;
        }

    }*/
}

