package com.shashank.assignment.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.ActivityResult;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.shashank.assignment.R;
import com.shashank.assignment.adapter.HomeAdapter;
import com.shashank.assignment.apicalling.ApiClient;
import com.shashank.assignment.apicalling.ApiInterface;
import com.shashank.assignment.model.DataModelResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewHomeActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_FLEXIBLE_UPDATE = 101;
    private AppUpdateManager appUpdateManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        appUpdateManager = AppUpdateManagerFactory.create(this);

        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(appUpdateInfo -> {

            Toast.makeText(this,"checking update",Toast.LENGTH_SHORT).show();
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // For a flexible update, use AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                // Request the update.
                requestUpdate(appUpdateInfo);
            }
        });

        callApi();
    }

    private void requestUpdate(AppUpdateInfo appUpdateInfo) {
        try {
            Toast.makeText(this, "Start update", Toast.LENGTH_SHORT).show();
            Log.d("requestUpdateMethod", "Start update");

            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.FLEXIBLE, //  HERE specify the type of update flow you want
                    this,   //  the instance of an activity
                    REQUEST_CODE_FLEXIBLE_UPDATE
            );
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(this, "from onActivityResult", Toast.LENGTH_SHORT).show();
        Log.d("onActivityResult", "enters in method on activity result");
        if (requestCode == REQUEST_CODE_FLEXIBLE_UPDATE) {
            switch (resultCode) {

                case Activity.RESULT_OK:
                    Toast.makeText(this, "Result Ok", Toast.LENGTH_SHORT).show();
                    Log.d("onActivityResult", "Result Ok");
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(this, "Result Canceled", Toast.LENGTH_SHORT).show();
                    Log.d("onActivityResult", "Result Canceled");

                    break;
                case ActivityResult.RESULT_IN_APP_UPDATE_FAILED:
                    Toast.makeText(this, "Result In App Update Failed", Toast.LENGTH_SHORT).show();
                    Log.d("onActivityResult", "Result In App Update Failed");

                    break;
            }
        }
    }

    private void callApi() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<DataModelResponse>> call = apiInterface.getDataModelResponse();

        call.enqueue(new Callback<ArrayList<DataModelResponse>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<DataModelResponse>> call, @NonNull Response<ArrayList<DataModelResponse>> response) {
                ArrayList<DataModelResponse> dataModelResponseArrayList = response.body();
                initView(dataModelResponseArrayList);
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<DataModelResponse>> call, @NonNull Throwable t) {
                Log.e("onFailure", t.getMessage());
            }
        });
    }


    private void initView(ArrayList<DataModelResponse> dataModelResponseArrayList) {
        RecyclerView rv = findViewById(R.id.recyclerView);
        TextView tvNoDataFound = findViewById(R.id.tv_no_data_found);

        if (dataModelResponseArrayList == null || dataModelResponseArrayList.size() <= 0) {
            rv.setVisibility(View.GONE);
            tvNoDataFound.setVisibility(View.VISIBLE);
        } else {
            rv.setVisibility(View.VISIBLE);
            tvNoDataFound.setVisibility(View.GONE);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(NewHomeActivity.this);
            rv.setLayoutManager(layoutManager);

            HomeAdapter homeAdapter = new HomeAdapter(NewHomeActivity.this, dataModelResponseArrayList);
            rv.setAdapter(homeAdapter);
        }
    }

}



