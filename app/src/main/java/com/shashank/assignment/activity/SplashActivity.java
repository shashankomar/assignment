package com.shashank.assignment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.tasks.Task;
import com.shashank.assignment.R;

public class SplashActivity extends AppCompatActivity {
//    private static final int RC_APP_UPDATE = 17362;
    private static final String TAG = SplashActivity.class.getSimpleName();
//    private AppUpdateManager mAppUpdateManager;
    /*private InstallStateUpdatedListener installStateUpdatedListener = state -> {

//       onStateUpdate(state);
        // Show module progress, log state, or install the update.
        Toast.makeText(this, "Install state update listener"+state, Toast.LENGTH_SHORT).show();

    };
*/

    ;
//    private Task<AppUpdateInfo> appUpdateInfoTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        checkForUpdate();// on create

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, NewHomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }
/*

    private void checkForUpdate() {
        Toast.makeText(this, "in app Update is checking", Toast.LENGTH_LONG).show();
        mAppUpdateManager = AppUpdateManagerFactory.create(this);

        mAppUpdateManager.registerListener(installStateUpdatedListener);

        appUpdateInfoTask = mAppUpdateManager.getAppUpdateInfo();
//
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                SplashActivity.this.startInAppUpdate(appUpdateInfo);
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, 30000);
            }

        });
    }

    private void startInAppUpdate(AppUpdateInfo appUpdateInfo) {
        Toast.makeText(SplashActivity.this, "Start in app update", Toast.LENGTH_LONG).show();
        try {
            mAppUpdateManager.startUpdateFlowForResult(appUpdateInfo,
                    AppUpdateType.FLEXIBLE,
                    this,
                    RC_APP_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_APP_UPDATE) {
            if (resultCode != RESULT_OK) {
                Toast.makeText(SplashActivity.this, "Update flow failed! Result code: " + resultCode, Toast.LENGTH_LONG).show();
                Log.d(TAG, "Update flow failed! Result code: " + resultCode);
//             start checking again if update fails or not installed
                checkForUpdate();//on Activity result
            } else {
                Toast.makeText(SplashActivity.this, "RESULT OK", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(" App is up to date")
                        .setCancelable(true)
                        .setPositiveButton("Okay", (dialog, id) -> {
                            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAppUpdateManager != null) {
            Task<AppUpdateInfo> appUpdateInfoTask = mAppUpdateManager.getAppUpdateInfo();

            appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
                if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                    Toast.makeText(this, "update completed click okay to restart", Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("update is downloaded! click restart for new features")
                            .setCancelable(true)
                            .setPositiveButton("Restart", (dialog, id) -> {
                                mAppUpdateManager.completeUpdate();
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAppUpdateManager.unregisterListener(installStateUpdatedListener);
    }

*/
/*
    @Override
    public void onStateUpdate(InstallState state) {
        switch (state.installStatus()) {

            case InstallStatus.DOWNLOADING:
                Toast.makeText(this, "Downloading", Toast.LENGTH_LONG).show();
                Log.d(TAG, String.valueOf(state.installStatus()));
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
//                Utility.showDialog(SplashActivity.this, String.valueOf(state.installStatus()));
                builder2.setMessage("update status downloading.")
                        .setCancelable(true)
                        .setPositiveButton("start app", (dialog, id) -> {
                            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        });
                AlertDialog alert2 = builder2.create();
                alert2.show();
                //show downloading UI
                break;

            case InstallStatus.DOWNLOADED:
                Toast.makeText(this, "Downloaded", Toast.LENGTH_LONG).show();
                Log.d(TAG, String.valueOf(state.installStatus()));
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("update is downloaded! click restart for new features")
                        .setCancelable(true)
                        .setPositiveButton("Restart", (dialog, id) -> {
                            mAppUpdateManager.completeUpdate();

                        });
                AlertDialog alert = builder.create();
                alert.show();
                break;

            case InstallStatus.FAILED:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "status Failed", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setMessage("In app update status Failed. please try again.")
                        .setCancelable(true)
                        .setPositiveButton("try again", (dialog, id) -> {
                            mAppUpdateManager.unregisterListener(installStateUpdatedListener);
                            checkForUpdate(); //onStateUpdate case Failed

                        }).setNegativeButton("cancel", (dialog, which) -> {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                });

                AlertDialog alert1 = builder1.create();
                alert1.show();
                //show failed UI/handle failed updated
                //start again

                break;

            case InstallStatus.CANCELED:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update canceled", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder5 = new AlertDialog.Builder(this);
                builder5.setMessage("In app update status cancelled.")
                        .setCancelable(true)
                        .setPositiveButton("go home", (dialog, id) -> {
                            mAppUpdateManager.unregisterListener(installStateUpdatedListener);
                            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();

                        });
                AlertDialog alert5 = builder5.create();
                alert5.show();

                //show canceled UI/handle failed update
                //open app
                break;

            case InstallStatus.INSTALLED:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update installed", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder4 = new AlertDialog.Builder(this);
                builder4.setMessage("In app update status Installed.")
                        .setCancelable(true)
                        .setPositiveButton("dialog dismiss", (dialog, id) -> {
                            dialog.dismiss();
                        });
                AlertDialog alert4 = builder4.create();
                alert4.show();

                break;

            case InstallStatus.INSTALLING:
                AlertDialog.Builder builder6 = new AlertDialog.Builder(this);
                builder6.setMessage("In app update status Installed.")
                        .setCancelable(true)
                        .setPositiveButton("go home", (dialog, id) -> {
                            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        });
                AlertDialog alert6 = builder6.create();
                alert6.show();


                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update installing", Toast.LENGTH_LONG).show();
                break;

            case InstallStatus.PENDING:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update pending", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder7 = new AlertDialog.Builder(this);
                builder7.setMessage("In app update status Installed.")
                        .setCancelable(true)
                        .setPositiveButton("", (dialog, id) -> {
                            dialog.dismiss();
                        });
                AlertDialog alert7 = builder7.create();
                alert7.show();

                break;

            case InstallStatus.REQUIRES_UI_INTENT:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update Requires UI intent", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder8 = new AlertDialog.Builder(this);
                builder8.setMessage("update Requires UI intent.")
                        .setCancelable(true)
                        .setPositiveButton("go home", (dialog, id) -> {

                            Intent intent8 = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent8);
                            finish();

                        });
                AlertDialog alert8 = builder8.create();
                alert8.show();

                break;

            case InstallStatus.UNKNOWN:
                Log.d(TAG, String.valueOf(state.installStatus()));
                Toast.makeText(this, "update unknown", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder9 = new AlertDialog.Builder(this);
                builder9.setMessage("update Requires UI intent.")
                        .setCancelable(true)
                        .setPositiveButton("go home", (dialog, id) -> {

                            Intent intent8 = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent8);
                            finish();

                        });
                AlertDialog alert9 = builder9.create();
                alert9.show();


                break;
        }
    }
*/
}
