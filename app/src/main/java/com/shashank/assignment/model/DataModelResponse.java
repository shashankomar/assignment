package com.shashank.assignment.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataModelResponse implements Serializable{

    @SerializedName("url")
    private String url;
    @SerializedName("name")
    private String name;
    @SerializedName("age")
    private String age;
    @SerializedName("location")
    private String location;
    @SerializedName("Details")
    private ArrayList<String> details = null;
    @SerializedName("bodyType")
    private String bodyType;
    @SerializedName("userDesire")
    private String userDesire;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<String> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<String> details) {
        this.details = details;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getUserDesire() {
        return userDesire;
    }

    public void setUserDesire(String userDesire) {
        this.userDesire = userDesire;
    }

}