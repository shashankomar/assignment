package com.shashank.assignment.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.shashank.assignment.R;
import com.shashank.assignment.activity.DetailActivity;
import com.shashank.assignment.model.DataModelResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<DataModelResponse> mDataModelResponseArrayList;

    public HomeAdapter(Context context, ArrayList<DataModelResponse> dataModelResponseArrayList) {
        mContext = context;
        this.mDataModelResponseArrayList = dataModelResponseArrayList;
    }

    @NonNull
    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutInflater = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_home, viewGroup, false);
        return new ViewHolder(layoutInflater);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapter.ViewHolder viewHolder, final int pos) {
        DataModelResponse dataModelResponse = mDataModelResponseArrayList.get(pos);

        if (dataModelResponse.getUrl()!=null){
            Picasso.get().load(dataModelResponse.getUrl())
                    .placeholder(R.drawable.ic_launcher_background).fit().into(viewHolder.mIvPerson);
        }else{
            Picasso.get().load(R.drawable.ic_launcher_background)
                    .placeholder(R.drawable.ic_launcher_background).fit().into(viewHolder.mIvPerson);

        }

        viewHolder.mTvName.setText(dataModelResponse.getName());
        viewHolder.mTvAge.setText(String.format("%s %s", mContext.getString(R.string.age), dataModelResponse.getAge()));
        viewHolder.mTvLocation.setText(dataModelResponse.getLocation());
    }

    @Override
    public int getItemCount() {
        if (mDataModelResponseArrayList == null) {
            return 0;
        } else {
            return mDataModelResponseArrayList.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView mIvPerson;
        private final TextView mTvName;
        private final TextView mTvAge;
        private final TextView mTvLocation;
        private final CardView cvHomeClick;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            mIvPerson = itemView.findViewById(R.id.iv_person);
            mTvName = itemView.findViewById(R.id.tv_name);
            mTvAge = itemView.findViewById(R.id.tv_age);
            mTvLocation = itemView.findViewById(R.id.tv_loc);
            cvHomeClick = itemView.findViewById(R.id.cv_home_click);
            cvHomeClick.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, DetailActivity.class);
            mContext.startActivity(intent);
        }
    }
}
